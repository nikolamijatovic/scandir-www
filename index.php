<?php
include_once 'scanDir/scanDir.php';
$scanner = new Scandir();
$files = $scanner->sortContent();
//$scan->dump($files);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title></title>
<!-- Bootstrap -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<link rel="stylesheet" href="scanDir/css/style.css" rel="stylesheet">
</head>
<body>

  <div class="page-header">
  	<div class="container">
  		<div class="row-fluid">
          <h1>
            <span class="glyphicon glyphicon-cloud" aria-hidden="true"></span>&nbsp;
            Localhost <small>apache server</small>
          </h1>
      	</div>
  	</div>
  </div>

  <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Virtual Domains</a></li>
            <li><a data-toggle="tab" href="#menu1">Directories</a></li>
            <li><a data-toggle="tab" href="#menu2">Files</a></li>
            <li><a data-toggle="tab" href="#menu3">Tools</a></li>

          </ul>

          <div class="tab-content">
            <!-- virtual domains tab-->
            <div id="home" class="tab-pane fade in active">
              <p>&nbsp;</p>
              <table class="table table-striped">
                <?php
                  foreach($files['domains'] as $domain){
                  ?>
                  <tr>
                    <td>
                      <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>&nbsp;
                      <?php echo $scanner->link($domain, true); ?>
                    </td>
                  </tr>
                  <?php } ?>
              </table>
            </div>
           <!-- directories tab -->
            <div id="menu1" class="tab-pane fade">
              <p>&nbsp;</p>
              <table class="table table-striped">
                <?php
                  foreach($files['dirs'] as $dir){
                  ?>
                  <tr>
                    <td>
                      <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>&nbsp;
                      <?php echo $scanner->link($dir); ?>
                    </td>
                  </tr>
                  <?php }?>
              </table>
            </div>
            <!-- files tab -->
            <div id="menu2" class="tab-pane fade">
              <p>&nbsp;</p>
              <table class="table table-striped">
                <?php
                  foreach($files['files'] as $file){
                  ?>
                  <tr>
                    <td>
                      <span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;
                      <?php echo $scanner->link($file); ?>
                    </td>
                  </tr>
                  <?php }?>
              </table>
            </div>
            <!-- tools tab -->
            <div id="menu3" class="tab-pane fade">
              <p>&nbsp;</p>
              <table class="table table-striped">
                <?php
                  foreach($files['tools'] as $tool){
                  ?>
                  <tr>
                    <td>
                      <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>&nbsp;
                      <?php echo $scanner->link($tool); ?>
                    </td>
                  </tr>
                  <?php } ?>
              </table>
            </div>

          </div><!-- end tab-content -->

        </div><!-- end col-md-6 -->
    </div> <!-- end row -->

  </div><!-- end container -->
</body>
</html>
