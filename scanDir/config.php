<?php

/*
|--------------------------------------------------------------------------
| Base Site Paths
|--------------------------------------------------------------------------
|
| 'base.url'       	=> 'domain_url.local', // base domain for this tool
|	'paths.dbschema' 	=> 'DbSchemas/', // Database migration schemas dir
|	'paths.scripts' 	=> 'Scripts/', // data manipulation scripts dir
|	'templates.path' 	=> './app/views', // teplates view dir
|
*/

return [
	'rootFilePath'    => dirname(__DIR__),
  //define tools we want to handle differente then regular files
  'tools' => ['phpmyadmin','info.php', 'adminer.php'],
  //define hidden files and dirs we dont want to show
  'hidden' => ['.', '..', 'scandir.php', 'index.php', 'scanDir'],
  // 'virtualHostFilespath' => '/etc/apache2/sites-enabled'; # change to suit your needs
  'virtualHostFilespath' => '/etc/nginx/sites-enabled', # change to suit your needs
];
