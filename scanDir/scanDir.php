<?php

/**
 * Class for scaning directories and parsing virtual host files
 * @TODO: add config file for the server type and parse virtual host files accordingly
 *
 */
class Scandir{

  protected $config;
  protected $dirPath;
  protected $server;
  protected $content = [];
  protected $tools;   //define tools we want to handle differente then regular files
  protected $hidden;  //define hidden files and dirs we dont want to show
  protected $vHostFilespath; # change to suit your needs

  public function __construct()
  {
    $this->server = $_SERVER['HTTP_HOST'];
    $this->config = include 'scanDir/config.php';

    $this->tools = $this->config['tools'];
    $this->hidden = $this->config['hidden'];
    $this->vHostFilespath = $this->config['virtualHostFilespath'];

    $this->dirPath = $this->config['rootFilePath'];
  }

  public function sortContent()
  {
    $this->content = $this->scanDirectory();

    return [
      'tools'   => $this->getTools($this->content),
      'dirs'    => $this->sortDirs($this->content),
      'files'   => $this->content,
      'domains' => $this->getVirtualDomains()
    ];
  }

  /**
   * Read content of directory
   *
   * @return array array holds two main keys by content type(dirs, files)
   */
  protected function scanDirectory(){
      return array_diff(scandir($this->dirPath), $this->hidden);
  }


  /**
   * Return all the specified tools
   * @return array
   */
  protected function getTools(){
      //return array_intersect($data, $this->tools);
      foreach ($this->content as $key => $value){
        if (in_array($value, $this->tools)){
            $content[] = $value;
            unset($this->content[$key]);
        }
      }
      return $content;
  }


  /**
   * Return only directories
   *
   * @return array contains only directories
   */
  protected function sortDirs(){
    foreach( $this->content as $key => $d ){
        if( is_dir($this->dirPath.DIRECTORY_SEPARATOR.$d)  )
        {
            $content[] = $d;
            unset($this->content[$key]);
        }    }

    return $content;
  }


  public function link($item, $localhost = false){
      if($localhost){
        $link = "<a href='http://{$item}'>{$item}</a>";
      } else {
        $link = "<a href='http://{$this->server}/{$item}'>{$item}</a>";
      }
      return $link;
  }


  /**
   * Helper methods
   */

    /**
   * Prints human-readable information about a variable.
   * if variable is array, it goes by print_r, else - var_dump.
   *
   * @param mixed $variable Variable
   * @param bool $name Variable name
   * @return void
   */
  public function dump( $variable, $name = false )
  {
      echo '<pre>'.PHP_EOL;

    // if we passed the name
    if ($name)
      echo $name.' = ';

    // here we divide output to array or any another variable type
    if (is_array($variable))
    {
      // again, name here also help with count elements
      if ($name)
        echo '('.count($variable).') ';

      print_r($variable);
    }
    else
    {
      var_dump($variable);
    }

      echo '</pre>'.PHP_EOL;
  }

  /**
   * Virtual hosts
   */
   protected function getVirtualDomains(){

     $domains = [];
     $hosts = $this->parseVirtualHostFiles();
     foreach ($hosts as $host) {
       if($host['ServerName']){
         $domains[] = $host['ServerName'];
       }
     }

     return $domains;
   }

   /**
    * Parse Apache VHosts to get list of Domains and Folder Paths
    */
   protected function parseVirtualHostFiles(){

      $conf_files = array_diff(scandir($this->vHostFilespath), $this->hidden);
      $info = array(); $x=0;

      foreach($conf_files as $conf_file){

          $Thisfile   = fopen($this->vHostFilespath .'/'.$conf_file, 'r') or die('No open ups..');

          while(!feof($Thisfile)){
              $line = fgets($Thisfile);
              $line = trim($line);

              $tokens = explode(' ',$line);

              if(!empty($tokens)){
                  if(strtolower($tokens[0]) == 'servername'){ # apache2 server
                      $info[$x]['ServerName'] = $tokens[1];
                  } else if(strtolower($tokens[0]) == 'server_name'){ # nginx server
                      $info[$x]['ServerName'] = rtrim($tokens[1],';');
                  }
                  if(strtolower($tokens[0]) == 'documentroot'){ # apache2 server
                      $info[$x]['DocumentRoot'] = $tokens[1];
                  } else if(strtolower($tokens[0]) == 'root'){ # nginx server
                        $info[$x]['DocumentRoot'] = $tokens[1];
                  }
                  if(strtolower($tokens[0]) == 'errorlog'){ # apache2 server
                      $info[$x]['ErrorLog'] = $tokens[1];
                  } else if(strtolower($tokens[0]) == 'error_log'){ # nginx server
                      $info[$x]['ErrorLog'] = $tokens[1];
                  }
                  if(strtolower($tokens[0]) == 'accesslog'){ # apache2 server
                      $info[$x]['AccessLog'] = $tokens[1];
                  } else if(strtolower($tokens[0]) == 'access_log'){ # nginx server
                      $info[$x]['AccessLog'] = $tokens[1];
                  }
                  if(strtolower($tokens[0]) == 'serveralias'){
                      $info[$x]['ServerAlias'] = $tokens[1];
                  }
              }
            }

        fclose($Thisfile);
        $x++;
      }

      return $info;
   }
}
//end class Scandir
